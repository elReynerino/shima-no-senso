# Soga no Iruka

- Alter: 22
- Aussehen:
  - Mönchstracht (Kriegerversion, also keine lange Kutte sondern Ballonhose) in dunklem Weinrot mit fuchs-orangener Schärpe
  - einfache Sandalen
  - Rucksack mit einzelnem Riemen
  - Kette bestehend aus ca. 1,5cm grossen Holzperlen, in der mitte eine Holzmedallie (ca. 3cm durchmesser), auf deren einer Seite ein Fuchskopf, auf der anderen das a-Mudra eingraviert sind
  - Bo Stab
  - Kopf ist fast kahl rasiert mit Pechschwarzen Haaren
  - Tattoo:
    - Rücken: Dharma Mandala
    <!-- ![alt text](ruecken_dharma_wheel_mandala.jpg) -->
    - linker Oberarm: Stilisiertes Yantra mit einigen Mudras
    <!-- ![alt text](arm_yantra.jpg) -->

## Familie: Soga Clan

(Klanoberhaupt ist einer der höchsten Kaiserlichen Minister)

### Kindheit

- Eltern: **Soga no Iname** und seine 4. Frau **Oane no Kimi**
- sehr distanzierte Beziehungen zu Vater und anderen Familienmitgliedern ( Inames 4. Frau wurde nach seinem Tod als Iruka 3 war eher zweitklassig behandelt), vor allem zu Halbbruder **Soga no Umako** der schon tief in den Staatsintrigen steckte, trotzdem sehr behütete und glückliche Kindheit im (neben-?) Palast gehabt zusammen mit Schwester **Suiko** (1 jahr älter)
- Traumatisches Ereignis: Mutter unter mysteriösen umständen gestorben (hatte Halbbruder **Soga no Umako** was damit zu tun???)
  - 🠊 daraufhin wurde Iruka mit 10 Jahren ins Kloster gesteckt.
  - Schwester **Suiko** wurde ein paar Monate darauf mit Shogun **Shotoku Taishi** verheiratet
  - von Kloster aus kaum mehr Kontakt und auch sonst sehr abgeschieden
- Klosterleben war dann sehr prägend mit einigen Senseis als Eltern-artige Vorbilder

### Iruka unbekannte geschichte

- Halbbruder Umako hat intrigen gegen Kaiser geschmiedet
- Tempel und Religion werden benutzt um die Macht zu sichern (Umako und Hatayasu haben gemeinsame Sache gemacht oder so?)

## Kloster: Asuka-Dera (Insel: Asuka)

- verehrung von **Inari** (Kami?) 🠊 Inari-Füchse (**Kitsune**) wohnen in der Tempelanlage
- Klosteroberhaupt **Soga no Hatayasu** ist ebenfalls vom Soga Clan (Onkel)
- relativ reiches kloster (auch durch Clan machenschaften), mit viel agrarwirtschaft auf insel und einem kleinen Hafen
- in den Bambuswäldern der Insel ist viel Spirituelle energie, man kann dort vielleicht sogar auf Kitsune Geisterwesen treffen
- Kloster ist sehr gemischt und alle Inselbewohner sind irgendwie in die Rituale eingebunden. Manche mehr Spirituell, manche mehr Praktisch (führen eher ein einfaches Dorfleben)
- Es werden dort viele Dharana Praktiken gelehrt, die dem Yoga und Kung Fu ähnlich sind. Auch alle möglichen anderen Meditationen und rituale für Spirituelle Energie und Geister
- Auf der ganzen Insel leben ca. 1000 leute.
- Topografie: TODO

### Shingon-shū ("Schule des wahren Wortes")

- Ziel: Buddha-werdung (**Vairocana**)
- Verwendung von Mantras und Mudras
- Methoden: durch Übungen eine außergewöhnliche Fertigkeit und Virtuosität erlangen 🠊 zu einem Dhyana ähnlichen höheren Bewusstseinszustand Zustand mit weniger Leid (Siddhis/Fähigkeiten)
- Kriegermönche (Sohei) dienen manchmal als eine art Schutzkämpfer der Spirituellen Geisterwelt, werden oft aber auch von der Führungsebene für politische Zwecke missbraucht
- Ritual: **a-omairi**
  - allgemeines Ritual mit fokus auf das a-Mudra
  - ist wie ein gebet, dementsprechend kann man für alles bitten
  - das Mudra und evtl. andere Spirituelle gegenstände/Zeichen/Mantras etc. werden verwendet um der Wirkung nachdruck zu verleihen
- Solodrill für leere Handfläche: **Siu Nim Tao**, ist  wie eine art tanz

### Klosterleben

- Onkel Oberhaupt hat Iruka meistens versucht zu ignorieren und  sich zu distanzieren
- Besonders gute Beziehung zu **Pathik Sensei**, ein alter blinder Mönch der Spezialist auf dem Chakren Gebiet und Yoga ist
- Iruka hat bei einem Handwerksmeistermönch **Jōchō Busshi** das Holzschnitzen/-bildhauen gelernt. Dort werden die ganzen Bildnisse und andere Ornament Sachen für den Tempel hergestellt.
- irgend ein Kitsune Geist hat Iruka auserwählt  und folgt ihm. Zeigt sich aber selten (das ist die hintergrund Lore zu dem Luck Feat: **"Inaris favored child"**)
- Viele texte gelesen und gebildet in Spiritualität, Selbstreflektion, Meditation, Supernatural lore
- **Kataoka Sensei (Tesshin)** (Martial Arts) typus hart aber fair. Hatten eine Art Vater Sohn Beziehung,  aber eher die Vorbildliche distanzierte Art und nicht emotional wie bei Pathik Sensei
- Beziehung zu anderen Mönchen meist distanziert (da denk ich mir nochmal weng was mehr aus)

## Neue Ereignisse

- Briefkontakt zu Schwester **Suiko** ist abgebrochen
    🠊 Halbschwester **Suiko** ist verschwunden? (war shogun ehemann in intrigen verwickelt?)
  - 🠊 persönliches Interesse: Suche nach Halbschwester, weil scheinbar niemand an Verbleib interessiert (Hatayasu vom Kloster rückt keine Infos raus)
- Pathik hat blockiertes Chakra in Iruka festgestellt und der Kitsune Geist der Iruka folgt ist unruhig geworden
  - 🠊 Iruka ist losgezogen um den Grund dafür und allgemein die Wahrheit zu finden
  - eine art pilgerreise ergibt sich
  - das blockierte Chakra ist das **Anahata (Herz/Feuer)**, In esoteric Buddhism, this Chakra is called Dharma and is generally considered to be the petal lotus of "Essential nature" and corresponding to the second state of Four Noble Truths.
  - Anahata is considered to be the seat of the Jivatman and Parashakti. In the Upanishads, this is described as a tiny flame inside the heart. Anahata is named as such because sages were believed to hear the sound (Anahata – comes without the striking of two objects together).[8] It is associated with air, touch and the actions of the hands.
  - Anahata is associated with the ability to make decisions outside the realm of karma. In Manipura and below, man is bound by the laws of karma and fate. In Anahata one makes decisions ("follows one's heart") based on one's higher self, not the unfulfilled emotions and desires of lower nature. As such, it is known as the heart chakra.[9] It is also associated with love and compassion, charity to others and psychic healing. Meditation on this chakra is said to bring about the following siddhis (abilities): he becomes a lord of speech, he is dear to women, his presence controls the senses of others, and he can leave and enter the body at will. 

## Spezieller Hintergrund: Mönch aus Asuka, Soga Clan Angehöriger

- prof. in Religion (geistiges training), Athletics (körperliches training)
- Besitz von Soga Insignie
- Sprache: Heilige Schrift/Sprache (Äquivalent von Sanskritt oder so?)

## Inspirationen

[Soga Clan](https://de.wikipedia.org/wiki/Soga_(Klan))

[Asuka-Dera Tempel](https://de.wikipedia.org/wiki/Asuka-dera)

[Shingon-Shu Buddhismus](https://de.wikipedia.org/wiki/Shingon-sh%C5%AB)

[Inari Kami (Shintoismus)](https://de.wikipedia.org/wiki/Inari_(Kami))

[Chakras](https://en.wikipedia.org/wiki/Chakra)

## Einfluss kürzlicher Begebenheiten

- "Kulturschock" nach langer weltfremd aufgewachsenen zeit
- sehr viel vorsichtiger anderen menschen (vor allem Samurai artigen leuten) zu vertrauen
- Iruka wundert sich, warum er so aufgewühlt ist, an dem Tod von Sokra kann es kaum liegen, er kannte sie ja Kaum, liegt es an dem Verrat? Oder an der gesamtsituation der überrannten Stadt? Durch seine distanz zum Herzchakra kann er das nicht gut auseinander halten
