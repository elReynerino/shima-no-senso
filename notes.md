# Notes

## Tenno Sennin (der weise Kaiser)

Die Insel Tokushima ist eine kleine Insel, aber bedeutende Insel zur Zeit des Kaiserreiches gewesen.  Die höchste Erhebung ist der 124 m hohe Noroshi-yama (のろし山) im Osten der Insel, auf dem ein Signalfeuer (noroshi) stand, dass bei Ankunft von ausländischem Schiff warnte. Seit vielen Generationen ist diese Insel der Stammsitz der Familie Tennō. Das gehaltene Territorium der Familie schrumpfte in den letzten 200 Jahren immer weiter bis nur noch diese Insel übrig blieb. Auch die Familie schrumpfte in dieser Periode immer weiter, bis nur noch der der letzte Spross Sennin (37 Jahre) übrigblieb. Geschwächt, isoliert und von Feinden umzingelt hielt dieser das Territorium für 17 Jahre gegen seine Feinde des Yamanaka-Klans. Heute sitzt Yamato Yamanaka der Enkel des großen Hijio Yamanaka als Statthalter auf der Insel Tokushima und Sennin Tennō ist gezwungen seinen Häschern zu entfliehen und Zuflucht auf Nokushima zu finden. Er hat geschworen sich die Insel und den Sitz seiner Familie zurückzuerobern. Auf der Suche nach Verbündeten trifft er mit seinem treuen Pferd Nobu (Vertrauen) einem kastanienbraunen Wallach mit einem Schiff auf der Insel ein.

Hafen: Mitsuhaya

## Kagayaku Mitsuko

- hält sich fern von Pferd
- redet von sich in 3. Person
- ist gross (min Geisha Sandalen größer als Iruka), hat blauen Kimono an, lange schwarze Haare und ist Hübsch, aber nicht übertrieben
- drachen mischling
- je mehr sie castet, desto mehr drache kommt raus
- kommt von **Ao Mizumi**:
  - "blaues wasser" -> kami fanden insel lw und haben was gemalt. fische, wasser und malen
- 

## Hamada Genjo

- **Sokra** war seine tante
- "diplomatische" missionen für clan gemacht
- 

## General Notes

- ein Daimio, **Nukushima** soll sich unterwerfen...
- Suche: Kajuk, gelbes Hemd, vor 7 Tagen losgezogen
- **Sokra**: Dame die Kajuk sucht
- **Takeru Kenji**: Strohhut mit kleinem Mann (Sakeverkäufer, Abenteurer)
- contact am Tempel: **Yamato Akiro**
- totes pferd an kreuzung: abviouly von Kajuk
- ...
- **Kajuk** überbrachte nachricht -> wichtiger botengan, mineralbeschaffung, zeitruck
- vor 5 tagen richtung richtung vulkanminen
- yamato schickt uns hin
- tintenklecks schmeckt bitter und modrig, riecht nach nix
- **Güreng** -> kontakt bei mienen
- brosche des tempels (schildkrötenpanzer)
- reisender mönch **Osamu** (erwähnt von Takeru)
- brücke kaputt -> müssen für Güreng 4 zwerge rüber holen
- brauchen mehr mineral für "Sie" als sonst
- zurück in mitsuchaja: hafen brennt
- hogo sha HQ: tenno und mitsuko bekommen hogo sha sigel
- reiten zum hafen, nachdem takeru kajuk zurück bringt
- hafensperrkettenturm stürmen und kette lichten
- flotte 15 grosse + 7-8 kleiner schiffe) liegt in abstand im hafen mit flagge ehem. shogun watanabe (kreis + 2 gespiegelte gingko blätter)
- 27 shogune -> 14 haben jetz inselstaaten
- **Watanabe** einer der 5 verschollenen/gefallenen? schogune
- **general Taliske** verbrennt leichen mit tatoos
- sehr organisierte **Sokra** versorgt leute im teehaus
- mit Takeru Kenji zu **Hogo Sha**
- nach vielen rätseln und kampf mit donaetello und michelangelo gegen klecks:
- höhle vergrössert sich, sokra erscheint
- mineral: **staub der götter** hat heilkräfte
- takeru kenji jahrhunderte???
- verltzung durch schwarzes pigment
- **Uruta** älterer rauschebart kenku
- Tenno köpft Sokra und verschwindet
- **Hogdal** (agrakor) mit hellebarde in teehaus
- **Kenji** heisst eig. **Takeru Maru**: früher Kami, trieb spässe mit anderen Kami, wollte ruhe auf Nukushima haben (Gott der Scherze und Tricks)
- **Pete** Boot Norden, **Osamu** Bibliothek? ?unleserlich?
- NO weg eingeschlagen
- General Taliske an hof auf weg getroffen (checkpoint), erklärt, dass **Tenno** zur Flotte geflüchtet ist
- **Pete** is koch auf zweimaster **Kupferpott**
- Ladeoffizier **Erik Gustavson**
- **Akami Sadato** im Krähennest / Navigatorin
- **Megumi Ayano** Steuerfrau / Befehl über Helfer
- **Ogwundu** Schiffshandwerker + Kanonier
- **Die schwestern** waren inseln der poesie/dichtens, bevor händlergilde übernommen hat
- Handelsgilde: 5 Untergruppierungen (eine davon die Schwestern)
- Hafen: **Ichiba Machi**
- ladung in disguse als flüchtende lady + anhang geschmuggelt
- im wald von **tokiwa** gelehrtenkloster
- **sannen saka** strasse (hauptstrasse durch die docks)
- pete empfiehlt gasthaus **erzählende zikade**: wirtin **Yao Nakamura**
- **sencho sato** hafenschutz beauftragter
- **soba no mikado** hat evtl vor 2-3 tagen soba nudel equipment entwendet (mühlsteine D=500mm, schneidebretter + aufbewahrungekisten, teigschale blau holz)
- **tanjiro** Yaos gehilfe
- drittpartei **unter dem mond**: durchaus mietbar
- das "Hindernis" **Toro Tadachi**
- Name in grüner Schüssel **Nekomaito**
- **Schloss der Tiefe**: Zeichen an lagerhaus wo repossession von Schüsseln, messern und Mülstein
- Sencho Sato veranlasst Razzia und entlohnt die Haika
- soba Kaiser dude: **Mr. Twig**, gehstock
- **Geschichtenerzähler Way** älter, halbelf, relativ blind, oberteil mit blauen linien
- trololol mit kindern vor miso king: hamada lässt kotz gift verteilen, nachdem er toto abgelenkt hat
- toro ist nett und beschreibt geschichte von Way
- nach schliessung folgen wir elf zu leerer werkstatt, wo er brief einwirft
- Brief: Dringendes treffene rbeten, zweiter vorfall innerhalb eines tages, wer shculd?
- verfolgen dude, der abends da rein und raus kam zu lagerhaus
- gespräch belauschen: bekomen unterstützung von grossmeister. das geforderte Treffen wird erwähnt. gebäude wird von anderen heute noch genutzt.
- sencho sato bericht erstattet, gehen mit 2 wachen (**Teloajus** dunkler zwerg und **Isa** halbelfin) zusammen zurück
- 
- okuri okami zähmer **kublai**
  - verabredet mit **Twig** in Ichibamachi
- beim kapmf gegen **Yukai: Okuri Okami** (Dämonen)  gekämpft, schienbar aggressiv gemacht
- note: handwerksbetrieb (seilerei) der attackiert werden sollte, **Grossmeister Niito**
- karte vom Hafen gefunden
- Note: zur Mittagszeit am **Tori Pier** Informant, von der **Unsichtbaren hand** (Gruppe im untergrund)
- **unter dem Mond** agiert  Hafenseite richtung innen (**Shibunwan**)
- Informant: alteingesessene kreise mögen jüngere gen (schloss d tiefe) nicht
- **Im Rahmen der Möglichkeiten** ramen bei den Aufzügen.
- Niito hat leerstehende gebäde gekauft
- viele parallele sabotage akte in der nacht passiert
- offiziell genehmigte handles reisende truppe im bereich der handelsgilde
- Geschichtenerzähler, Zwerg, trägt Hakama, waldgrünes Oberteil
- Geschichte dder Schwestern:
	- Ichiba Machi:
	- ursprüngliche den künsten (gesang, dichtung mehr) gewidmet
	- alles lief langsamer
	- zwei städte ursprünglich mit langem spalt dazischen
	- spalt ist heute überbaut
	- insel von kami geschaffen, der geschihten hören liebte, versah die insel mit gabe der inspiration
	- andere kami kamen um zu lauschen und zu teilen.
	- handel kam
	- bis heute, wo künstlerischen fast vergessen ist
- **Osamu** kam auch zu den Schwestern
- **Akademie von Tokewa** evtl. mehr infos (im Wald von **Tokaiwa**)
- 
- Großmeister Dude **Niito** chefe? + kauft leere gebäude auf
- Halb-Ork informant von **unsichtbare hand** am tori? pier
- **hosadas schreibwaren**
- **balthasars magische bastelstube**
- **lauf der dinge** spez. für fernkampf
- Auf Weg zum Kloster Kampf gegen **Kappa** (froschn) verhindert
- Am Kloster begrüsst uns ein Vogelmönch mit einem Mord an einem anderen Mönch mit Tintenklecks im Bein
- **Kanshinin Ashita** (wervogel)
- **Kami Shue** (Humanoid mit Singvogelkopf + Schreibfeder mit Papier) erster schreiber der schwestern
- noch **grössere bibliothek**: Kais. Staatsbib (Abgebrannt), **Bücherturm der Arakokra** (Auf insel der Arakokra), **Bib der letzten Kirschblüte (最後の桜 Saigo no sakura)** (Mystischer Ort, von osamu zusammengetragen)
- wandteppoich mit abbildung von Kami Fuei auf stein gegenüber von **Yokai** mit schwarzem umhang der fliessend ind Yokai (Dämonen?) abbildungen übergeht
- Goro Takenashi mönch am boden: erzählt von schatten der dinge aufgesaugt hat und zu menchenähnlicher figur wurde
- **Shue** vs. **Yaminosaka, schreiber der dunkelheit**, auch schöpfer des bösen/Yokai oder gegenpol auf waage (wandteppich)
- BBEG (Grosser humanoider blob der **Yamato Akiro** aussugt) ist von **Yaminosaka** geschaffenes wesen
- Namedrop "Lord **Yamiwa** wird stolz sein wg. Ziele früher erreichen"
- coolaer name: **Saul Goodman**
- wandteppich:
  - stellt begegnung zw. yaminosaka und shue dar
  - felskugel 3m, kordel aussenrum
  - darauf rabe
  - link wald wasser
  - rechts strand
  - figur mit schwarzer robe
- geschichte:
  - insel gewidmet f die künste des erzählens
  - ein kami betrachtete insel als unvollendet (yaminosaka)
  - brachte dämonen mit auf insel (gleichgewicht bringen)
  - "alles schöne muss durch leid und prüfung verdient werden"
  - schuf yokai (dämonen) durch schreiben
  - kami stellten sich gegen ihn
  - er wurde am ende verbannt nach **Kyo** (die leere zwischen allem anderen, der eigentliche andere ort der kami, "der leere")
- Yamato Akiro:
  - "dunkle" tinte (dunkle magie)
  - tinte ist nicht tot solange sie flüssig ist
  - kann neue kraft erlangen / vervielfältigen
    - blut
    - weitere tinte/pigmente
  - grosser tintenklecks erschienen + ausgebreitet/repliziert
  - tinte ist amorph im freien
  - klettert überall
  - empfindlich geg. sonnenlicht + feuer
  - immun gegen spitz + scharf
  - schleim wollte avatar von yaminosaka werden, dämonen wiedererwecken usw.
  - shogun **watanabe** ist general **yamiwa**
- **Die grossen Jagden**: vor 300-400 Jahren, dämonensäuberung von kaiserlicher seite und glaubensrichtungen unterstützt
- **Baronie der fallenden Blütenblätter** im nordern auf den schwester, westliche insel
  - überbleibsel der dämonenjäger
  - "kompliziert"
  - dort gibt es untergeschlüpfte Yokai (wind weht zu **Kanshinin Ashita**, werrabe)
- **handelsgilde = Hansel Gilde**
- "im notfall zum tausch": Buch "Gedichte aus den frühen Tagen Ichibamachis" (ursprünglicher wortlaut des namens), zusammengetragen vom geschichtenerzähler wei, signiert
- **Zurück in Stadt:**
- in näh von **Gestreifter Greif**(kleiner Yakitori Spiess stand) zum Kontakt
- **Vertreter d. Handelsgilde Ishi Mukami**
- suchen kenku im zusammenhang mit der verschleierung von den vorkommnissen im hafenviertel (nasch aussage von taddl)
- **Okarims Opulente Outfits** (Gnom)
- Fähre nach kaichiKodai:
  - Meditationsübungen mit Mitsuko
  - Geschichtenerzähler: Osamu
- Osamus Geschichte:
  - ahornblatt nach windhauch -> reise sich die schöpfung der kami anzuschauen
  - begegnete Kamis der inseln
  - immer wieder zu ahorn zurück -> erzählt ahorn die erlebnisse -> baum wuchs
  - nach tausend jahren, grösster baum -> tempel des wissens
  - kami haben fremde schöpfung gesucht -> missfallen über tempel des wissens mit gehiemnissen
  - unter anderem **pforten von kyo**
  - baum und tempel des wissens widerstand den kami -> machten den tempel unauffindbar/erreichbar (strömungen, winde, wächter usw.)
  - osamu von insel verbannt -> ein kami: ungleichgewicht -> **yaminosaka** schuf einzelnen weg zur insel
  - osamu fand auf sienen reisen iwann ein ahornblatt das er erkannte -> machte sich erneut auf suche
- herberge: **voller wagen** mit **tanja** und einem goliath als wirte
- **Toro** (crimson hunter) und **Kanopi**, wächter der Baronie greifen in Kampf mit Magiern und Illusionshund (angeblich von Handelsgilde)
- Auftragsschreiben: Leuten Angst machen, auch mit gewalt, immer mind. einen leben lassen + vorsicht vor blind gewalttätigen Hunden nach Verzauberung
- **Oberu** der Gastwirt der Baronin
- Baronie, Stadt:
  - 1 pplatz +pagone
  - 2 Palast? mit garten
  - 3 trainingsplatz
  - 4 ausbildungsgebäude
  - 5 **Tublai Kotan** alchemist
    - großer stark übergew. mensch
    - schiefes gebäude, oft gefixt
  - 6 **Iroha** freundin von Toro und schmiedin

  - 7 blumengarten der geehreten **Hanabi**
    - geisha mit ultra hals
    - wunderschöner aber giftiger garten
  - 8 **Uchi-san** teehaus
    - gnom mit spitzer kochmütze
    - gebäck spezialitäten
  - 9 bildhauerin und verziererin **Sasuke**
    - halb menschengrösse, tanuki (marderhund)
    - bildhauer und maler shop, mit vielen laufenden projekten
  - gemischtwarenhändler **Toko Ichiban**
    - **Ken Totsuin**: klein, breites ges. vorstehende augen, langer mantel, nicht menschl.


Beim Gebet am schrein ( erster tag in der stadt) schließt Du kurz die Augen und in diesem Moment siehst Du folgendes:
Einen runden Stein der in einem Wald steht und ein Rabe, welcher oben auf sitzt. Draum läuft ein Fuchs und scheint durch den Stein hindurch zu gehen. Danach legt sich ein Schatten auf den Stein und es erscheinen dunkle Wolken am Himmel.

visionen ausgetauscht:
- alle hatten vision von rundem stein.
  - mitsuko mit raben drauf, der geschichten erzählt mit drachenschildkröte, dann kommen dunkle wolken und blitz zerteilt den stein
  - hamada mit schwarzen federn und schwarze? suppe fliesst aus dem stein

im wald mit sasuke und mitsuko
- suche nach magisch geeigneten bäumen, die nicht blühen
- mitsuko versucht tanzend die drachenschildkröte für die zeichnung von sasuke zu beschreiben

hamada besorgt alchemiematerialien aus lager

- trainingsparcours bei dämonenjäger

- gehen mit toru zu JVA (aussenposte, tempel/schreine, verwittert)
  - genjo ist sauer, weil iruka es zugelassen hat, dass mitsuko den gefangenen vll foltert
  - iruka hat kleines gespräch mit ihm und findet heraus, dass **mr. twig die okuri okami kauft**
  - mitsuko war richtig nett zu iruka, als er siene eigenen methoden verwenden wollte und hat den schildkrötentalismann ausgeliehen

- heist: gefangenen "rausholen"
  - er hat unterschlupf an landzunge im osten
  - arbeitet mit **mr twig**
  - zwei tengu **Kodaku** und **Fuji...** überweältigen gefangenen
  - sie sehen MItsuko, hamada und iruka vmtl. nicht
  - Toru schickt mitarbeiter hin und kümmert sich um den "notfall"
  - wir gehen zurück

- nächster morgen:
  - toru und kanopi verkünden urteil für gefangenen
  - danach zur baronin (audienz)
  - zwei weisse füchse + eine grosse siebenschwänzige füchsin